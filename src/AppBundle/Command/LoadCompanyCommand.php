<?php

namespace AppBundle\Command;

use AppBundle\Entity\CompanyEntity;
use AppBundle\Entity\IndustryEntity;
use AppBundle\Entity\SectorEntity;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadCompanyCommand extends ContainerAwareCommand
{
    const DOMAIN = 'http://www.nasdaq.com';
    const PATH_LOAD = '/screening/companies-by-name.aspx?&render=download';

    protected function configure()
    {
        $this->setName('load-company:csv')
            ->setDescription('Load companies')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2G');

        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $client = new Client([
            'base_uri' => self::DOMAIN,
            'timeout'  => 5.0,
        ]);

        $data = $client->get(self::PATH_LOAD);
        $row = $data->getBody()->getContents();

        $lines = explode(PHP_EOL, $row);
        $head = array_map('strtolower', str_getcsv($lines[0]));
        $column = count($head);
        $allRows = count($lines) - 1;

        $progressBar = new ProgressBar($output, ($allRows + 1));
        $progressBar->start();
        foreach ($lines as $key => $line) {
            if ($key === 0) {
                continue;
            }
            $rawData = str_getcsv($line);
            if (count($rawData) !== $column) {
                continue;
            }
            $item = array_combine($head, array_map('trim', $rawData));

            foreach ($item as &$field) {
                if ($field == 'n/a') {
                    $field = null;
                }
            }
            $sector = null;
            $industry = null;
            if (
                $item['sector']
                && (!$sector = $em->getRepository("AppBundle:SectorEntity")->findOneBy([
                    'name' => $item['sector']
                ]))
            ) {
                $sector = new SectorEntity();
                $sector->setName($item['sector']);
                $em->persist($sector);
            }
            if (
                $sector
                && $item['industry']
                && (!$industry = $em->getRepository("AppBundle:IndustryEntity")->findOneBy([
                    'name' => $item['industry'],
                    'sector' => $sector
                ]))
            ) {
                $industry = new IndustryEntity();
                $industry->setName($item['industry']);
                $industry->setSector($sector);
                $em->persist($industry);
            }
            if (!$company = $em->getRepository("AppBundle:CompanyEntity")->findOneBy(['symbol' => $item['symbol']])) {
                $company = new CompanyEntity();
                $company->setSymbol($item['symbol']);
            }
            $company->setName($item['name']);
            $company->setLastSale($item['lastsale']);
            $company->setMarketCap($item['marketcap']);
            $company->setIPOyear($item['ipoyear']);

            if ($sector) {
                $company->setSector($sector);
            }
            if ($industry) {
                $company->setIndustry($industry);
            }
            $company->setSummaryQuote($item['summary quote']);

            $em->persist($company);
            $em->flush();
            $progressBar->advance();
        }
        $progressBar->finish();

        $output->writeln('Complete.');
    }

}