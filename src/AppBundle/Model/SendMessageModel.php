<?php
namespace AppBundle\Model;

use AppBundle\Entity\CompanyEntity;

class SendMessageModel
{
    private $swiftMailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->swiftMailer = $mailer;
    }

    public function sendInfoAboutCompany($email, CompanyEntity $company, $content)
    {
        $message = (new \Swift_Message($company->getName()))
            ->setFrom('test@test.com')
            ->setTo($email)
            ->setBody(
                $content
            );
        $this->swiftMailer->send($message);
    }

    public function createContentMessageForMainForm(\DateTime $dateStart, \DateTime $dateEnd)
    {
        return "From {$dateStart->format('Y-m-d')} to {$dateEnd->format('Y-m-d')}";
    }
}