<?php

namespace AppBundle\Model;

use AppBundle\Entity\CompanyEntity;
use GuzzleHttp\Client;

class ChartModel
{
    const TEMPLATE_URL = '/api/v3/datasets/WIKI/{company}.csv?order=asc&start_date={dateStart}&end_date={dateEnd}';
    const DOMAIN = 'https://www.quandl.com';


    public function parse(CompanyEntity $company, \DateTime $dateStart, \DateTime $dateEnd)
    {
        $companySymbol = $company->getSymbol();
        $client = new Client([
            'base_uri' => self::DOMAIN,
            'timeout'  => 5.0,
        ]);
        $url = strtr(
            self::TEMPLATE_URL,
            [
                '{company}' => $companySymbol,
                '{dateStart}' => $dateStart->format('Y-m-d'),
                '{dateEnd}' => $dateEnd->format('Y-m-d')
            ]
        );
        $data = $client->get($url);
        $row = $data->getBody()->getContents();
        $lines = explode(PHP_EOL, $row);
        $head = array_map('strtolower', str_getcsv($lines[0]));
        $column = count($head);
        $result = [];
        $dataForTable = [];
        foreach ($lines as $key => $line) {
            if ($key === 0) {
                continue;
            }
            $rawData = str_getcsv($line);
            if (count($rawData) !== $column) {
                continue;
            }
            $item = array_combine($head, array_map('trim', $rawData));
            $dataForTable[] = $item;
            $result[$item['date'] . ' open'] = (float)$item['open'];
            $result[$item['date'] . ' close'] = (float)$item['close'];
        }

        return [$result, $dataForTable];
    }
}