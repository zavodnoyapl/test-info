<?php

namespace AppBundle\Controller;

use AppBundle\Form\MainType;
use GuzzleHttp\Exception\RequestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(
            MainType::class,
            null,
            ['attr' => ['id' => 'form_info']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            try {
                list($chartData, $data) = $this->get('app.parse.chart')->parse(
                    $formData['company'],
                    $formData['dateStart'],
                    $formData['dateEnd']
                );
                $this->get('app.email.message')->sendInfoAboutCompany(
                    $formData['email'],
                    $formData['company'],
                    $this->get('app.email.message')->createContentMessageForMainForm(
                        $formData['dateStart'],
                        $formData['dateEnd']
                    )
                );
                return $this->render('@App/main/chart.twig', [
                    'companyName' => $formData['company']->getName(),
                    'data' => json_encode(array_values($chartData)),
                    'label' => json_encode(array_keys($chartData)),
                    'tableData' => $data
                ]);
            } catch (RequestException $e) {
                $form->addError(new FormError('Request Error'));
            }
        }
        return $this->render('@App/main/main.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/search-company", name="search_company")
     */
    public function searchCompanyAction(Request $request)
    {
        $em = $this->get('doctrine')
            ->getManager()
        ;
        $result = $em->getRepository("AppBundle:CompanyEntity")->searchCompany($request->get('q'));

        return new JsonResponse($result);
    }
}
