<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class MainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add('email',
                EmailType::class,
                [
                    'required' => true,
                    'label' => 'Email',
                    'constraints' => [
                        new Email()
                    ]
                ]
            )
            ->add('company', Select2EntityType::class, [
                'multiple' => false,
                'remote_route' => 'search_company',
                'class' => 'AppBundle\Entity\CompanyEntity',
                'primary_key' => 'id',
                'text_property' => 'symbol',
                'minimum_input_length' => 1,
                'page_limit' => 10,
                'allow_clear' => true,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'en',
                'placeholder' => 'Select a company',
                'required' => true,
                'label' => 'Company'
            ])
            ->add('dateStart',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'Date Start',
                    'attr' => [
                        'class' => 'datetimepicker',
                    ],

                    'constraints' => [
                        new NotBlank([
                          'message' => 'form.error.not_blank',
                        ]),
                        new Date(),
                        new LessThanOrEqual([
                          'value' => new \DateTime(date('Y-m-d')),
                            'message' => 'Date should be less or equal ' . date('Y-m-d'),
                        ]),
                    ],
                ]
            )
            ->add('dateEnd',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'Date End',
                    'attr' => [
                        'class' => 'datetimepicker',
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'form.error.not_blank',
                        ]),
                        new Date(),
                        new LessThanOrEqual([
                            'value' => new \DateTime(date('Y-m-d')),
                            'message' => 'Date should be less or equal ' . date('Y-m-d'),
                        ]),
                    ],
                ]
            )
            ->add('Submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn_success'
                ]
            ])
        ;

        $builder->get('dateStart')->addModelTransformer(new CallbackTransformer(
            function (\DateTime $date = null) {
                if (!$date) return '';
                return $date->format('Y-m-d');
            },
            function ($dateString) {
                return (new \DateTime($dateString))->setTime(0,0,0);
            }
        ));

        $builder->get('dateEnd')->addModelTransformer(new CallbackTransformer(
            function (\DateTime $date = null) {
                if (!$date) return '';
                return $date->format('Y-m-d');
            },
            function ($dateString) {
                return (new \DateTime($dateString))->setTime(0,0,0);
            }
        ));

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $rangeStart = new \DateTime($data['dateStart']);
                $rangeEnd = new \DateTime($data['dateEnd']);
                if($rangeEnd && $rangeStart && $rangeEnd < $rangeStart){
                    $event->getForm()->addError(new FormError('End date should be after start date!'));
                }
            });
    }
}