<?php

namespace AppBundle\Repository;

use AppBundle\Entity\CompanyEntity;
use Doctrine\ORM\EntityRepository;

class CompanyRepository extends EntityRepository
{
    public function searchCompany($text, $limit = 10)
    {
        /**
         * @var $data CompanyEntity[]
         */
        $data = $this->getEntityManager()
            ->getRepository("AppBundle:CompanyEntity")
            ->createQueryBuilder('c')
            ->where('c.symbol LIKE :text')
            ->setParameter('text', "{$text}%")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
        $result = [];
        foreach ($data as $elem) {
            $result[] = [
                'id' => $elem->getId(),
                'text' => $elem->getSymbol()
            ];
        }

        return $result;
    }
}