<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Companies
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 * @UniqueEntity("symbol")
 */

class CompanyEntity {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=10, unique=true, nullable=false )
     */
    private $symbol;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false )
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="last_sale", type="float", nullable=true )
     */
    private $lastSale;

    /**
     * @var string
     *
     * @ORM\Column(name="market_cap", type="string", length=255, nullable=true )
     */
    private $marketCap;

    /**
     * @var int
     *
     * @ORM\Column(name="ipo_year", type="smallint", length=4, nullable=true)
     */
    private $IPOyear;


    /**
     * @ORM\ManyToOne(targetEntity="SectorEntity")
     * @ORM\JoinColumn(name="sector_id", referencedColumnName="id")
     */
    private $sector;

    /**
     * @ORM\ManyToOne(targetEntity="IndustryEntity")
     * @ORM\JoinColumn(name="industry_id", referencedColumnName="id")
     */
    private $industry;

    /**
     * @var string
     *
     * @ORM\Column(name="summary_quote", type="string", length=255, nullable=true )
     */
    private $summaryQuote;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getLastSale()
    {
        return $this->lastSale;
    }

    /**
     * @param float $lastSale
     */
    public function setLastSale($lastSale)
    {
        $this->lastSale = $lastSale;
    }

    /**
     * @return string
     */
    public function getMarketCap()
    {
        return $this->marketCap;
    }

    /**
     * @param string $marketCap
     */
    public function setMarketCap($marketCap)
    {
        $this->marketCap = $marketCap;
    }

    /**
     * @return int
     */
    public function getIPOyear()
    {
        return $this->IPOyear;
    }

    /**
     * @param int $IPOyear
     */
    public function setIPOyear($IPOyear)
    {
        $this->IPOyear = $IPOyear;
    }

    /**
     * @return mixed
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * @param mixed $sector
     */
    public function setSector(SectorEntity $sector)
    {
        $this->sector = $sector;
    }

    /**
     * @return IndustryEntity
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * @param IndustryEntity $industry
     */
    public function setIndustry(IndustryEntity $industry)
    {
        $this->industry = $industry;
    }



    /**
     * @return string
     */
    public function getSummaryQuote()
    {
        return $this->summaryQuote;
    }

    /**
     * @param string $summaryQuote
     */
    public function setSummaryQuote($summaryQuote)
    {
        $this->summaryQuote = $summaryQuote;
    }


}